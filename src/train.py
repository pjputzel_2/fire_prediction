import os
import logging
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report, roc_curve, mean_squared_error

try:
    from matplotlib import pyplot as plt
except:
    logging.warn('Skipping pyplot import')

from time import gmtime, strftime
import pickle as pkl

from utils import loaders
from evaluation import compute_f1_scores

from models.logistic_regression import LogisticRegression
from models.Grid_CRF import GridCRF

def save_model_and_results(model, params, results, fp, tp, dest_dir):
    os.makedirs(dest_dir)

    dest_path = os.path.join(dest_dir, 'model.pkl')
    loaders.save_pickle(model, dest_path)

    dest_path = os.path.join(dest_dir, 'params.txt')
    with open(dest_path, 'w+') as out:
        out.write(str(params))

    dest_path = os.path.join(dest_dir, 'results.txt')
    with open(dest_path, 'w') as fout:
        fout.write(results)

    dest_path = os.path.join(dest_dir, 'roc.png')
    try:
        plt.plot(fp,tp)
        plt.savefig(dest_path)
    except:
        logging.warn('Skipping generation of ROC curve.')

def train(params):
    src_path = os.path.join(params['data_dir'], params['model']['src_path'])
    experiment_name = params['model']['experiment_name'] + '.' + strftime("%Y%m%d_%H%M%S", gmtime()) + '.' + str(np.random.randint(999))
    dest_dir = os.path.join(params['model']['model_dir'], experiment_name)

    # Instantiate model
    model_type = params['model']['model_type']
    if model_type == 'logistic_regression':
        model = LogisticRegression(params)
        X, y = loaders.load_numpy(src_path)
    elif model_type == 'grid_crf':
        model = GridCRF(params)
        X, y = loaders.load_numpy(src_path)

    else:
        raise ValueError('Unknown model type "%s"' % model_type)
    # Split data
    test_frac = params['model']['test_frac']
    random_seed = params['model']['random_seed']

    # Use a fixed number of training examples
    sample_size = params['model'].get('sample_size', None)

    if sample_size:
        print X.shape
        X = X[0:sample_size]
        y = y[0:sample_size]

    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=test_frac, random_state=random_seed)

    # Train model
    model.fit(X_train, y_train)

    # Basic model evaluation on test data
    pred = model.predict(X_test)

    results = ''

    # F1 Scores
    f1_scores = compute_f1_scores(y_test, pred)
    results += str(f1_scores)

    if model_type != 'grid_crf':
        pred_prob = model.predict_proba(X_test)[:,1]
        fp, tp, thresh = roc_curve(y_test, pred_prob)

    # Classification report
    class_report = classification_report(y_test, pred)
    results += '\n' + str(class_report)

    # MSE
    mse = mean_squared_error(y_test, pred)
    results += '\n' + 'MSE: ' + str(mse)

    logging.info(results)
    
    # Save model
    save_model_and_results(model, params, results, fp, tp, dest_dir)


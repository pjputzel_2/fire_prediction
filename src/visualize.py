import pickle
from matplotlib import pyplot as plt
import numpy as np
import matplotlib.animation as animation
import sys


def animate_classifier(data_path, model_path, cluster, dest_path):
    with open(data_path, 'rb') as fin:
        tile_dict = pickle.load(fin)

    X, Y, land_cover = tile_dict['x'], tile_dict['y'], tile_dict['land_cover']

    with open(model_path, 'rb') as fin:
        model = pickle.load(fin)

    #clust1 = X[cluster]
    clust1 = X[cluster][:,-1,:,:]
    clust2 = land_cover[cluster]
    clust3 = Y[cluster]
    #clust4 = model.predict_cluster(X[cluster], Y[cluster], land_cover[cluster])
    clust4 = model.predict_cluster_prob(X[cluster], Y[cluster], land_cover[cluster])

    #animate_cluster_pair(clust1, clust2, dest_path, slow=1)
    animate_cluster_quad(clust1, clust2, clust3, clust4, dest_path, slow=1)

def animate_cluster_pair(clust1, clust2, dest_path, slow=1):
    fig, ax = plt.subplots(1,2)

    im1 = ax[0].imshow(clust1[0], animated=True)
    im2 = ax[1].imshow(clust1[0], animated=True)
    imgs = [im1, im2]

    def updatefig(num):
        #_ = fig.suptitle('%d' % num)
        ind = int(num/slow)
        imgs[0].set_array(clust1[ind])
        imgs[1].set_array(clust2[ind])
        return imgs,

    ani = animation.FuncAnimation(fig, updatefig, frames=len(clust1)*slow, interval=700)
    
    writer = animation.ImageMagickWriter(fps=1)
    ani.save(dest_path, writer=writer)

def animate_cluster_quad(clust1, clust2, clust3, clust4, dest_path, slow=1):
    fig, ax = plt.subplots(2,2)

    im1 = ax[0][0].imshow(clust1[0], vmin=np.min(clust1), vmax=np.max(clust1), animated=True)
    im2 = ax[0][1].imshow(clust2[0], vmin=np.min(clust2), vmax=np.max(clust2), animated=True)
    im3 = ax[1][0].imshow(clust3[0], vmin=np.min(clust3), vmax=np.max(clust3), animated=True)
    im4 = ax[1][1].imshow(clust4[0], vmin=np.min(clust4), vmax=np.max(clust4), animated=True)
    imgs = [im1, im2, im3, im4]

    def updatefig(num):
        _ = fig.suptitle('%d' % num)
        ind = int(num/slow)
        imgs[0].set_array(clust1[ind])
        imgs[1].set_array(clust2[ind])
        imgs[2].set_array(clust3[ind])
        imgs[3].set_array(clust4[ind])

        return imgs,

    ani = animation.FuncAnimation(fig, updatefig, frames=len(clust1), interval=700)

    ani.save(dest_path, writer='imagemagick')


if __name__ == '__main__':
    animate_classifier(sys.argv[1], sys.argv[2], int(sys.argv[3]), sys.argv[4])

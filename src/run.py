import click
import logging

from utils.params import parse_params
from utils.processing import process_data
from train import train


def run(params):
    operation = params['operation']
    if operation == 'process':
        process_data(params)
    elif operation == 'train':
        train(params)
    else:
        raise ValueError('Unknown operations "%s"' % operation)


@click.command()
@click.argument('params_path', type=click.Path(exists=True))
@click.option('--log', default='INFO')
def main(params_path, log):
    # Setup logging
    log_fmt = '%(asctime)s - %(levelname)s - %(message)s'
    logging.basicConfig(level=getattr(logging, log.upper()), format=log_fmt)

    # Parse params from file
    params = parse_params(params_path)
    logging.info('Params: %s' % params)
    print params
    # Run program
    run(params)


if __name__ == '__main__':
    main()

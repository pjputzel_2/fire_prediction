import numpy as np
import itertools

import loaders

class LandCoverDeg(object):
    def __init__(self, src_path):
        self.data = loaders.load_pickle(src_path)
        self.grid_cell_size = 180./len(self.data)

    def get_tile(self, top_left_lat, top_left_lon, height, width, grid_cell_size, units_to_lat, units_to_lon):
        points = []

        lats = np.linspace(top_left_lat, top_left_lat-(height*units_to_lat(grid_cell_size)), height)
        vert_inds = np.int32((90 - lats)/self.grid_cell_size)

        for i in range(height):
            lons = np.linspace(top_left_lon, top_left_lon+(width*units_to_lon(grid_cell_size, lats[i])), width)
            horiz_inds = np.int32((lons+180)/self.grid_cell_size)

            row = map(lambda x: (vert_inds[i], x), horiz_inds)
            points += row


        #points = list(itertools.product(vert_inds, horiz_inds))
        points_transpose = zip(*points)

        land_cover = self.data[points_transpose]
        tile = np.reshape(land_cover, (height, width))

        return tile

class LandCoverMeters(LandCoverDeg):
    pass

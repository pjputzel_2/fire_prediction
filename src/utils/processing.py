import os
import pickle
import numpy as np
import pandas as pd
import logging
import collections
from sklearn import preprocessing
import loaders
from land_cover import LandCoverDeg, LandCoverMeters

DEGREES_TO_KM = 111

def round_to_value(n, value):
    return round(n/value)*value

def lon_to_km(lon, lat):
    return 111.32 * np.cos(np.deg2rad(lat)) * lon

def km_to_lon(dist, lat):
    return dist / (111.32 * np.cos(np.deg2rad(lat)))

def lat_to_km(lat):
    return DEGREES_TO_KM*lat

def km_to_lat(dist):
    return dist / float(DEGREES_TO_KM)

def deg_identity(deg, lat=None):
    return deg

def process_data(params):
    src_path = os.path.join(params['data_dir'], params['process']['src_path'])
    dest_path = os.path.join(params['data_dir'], params['process']['dest_path'])

    process_type = params['process']['process_type']
    if process_type == 'create_tiles' or process_type == 'create_tiles_deg':
        D = params['process']['num_prev_days']
        N = params['process']['num_pts']

        cell_type = 'km' if process_type=='create_tiles' else 'deg'
        grid_cell_size = params['process']['grid_cell_size']
        deg_precision = params['process']['deg_precision']

        land_cover_src = params['process'].get('land_cover_src_path', None)
        if land_cover_src:
            land_cover_src = os.path.join(params['data_dir'], land_cover_src)
            land_cover_type = params['process']['land_cover_type']
        else:
            land_cover_type = None

        include_history = params['process']['include_history']

        min_days = params['process']['min_days']
        min_detections = params['process']['min_detections']

        percentile = params['process']['percentile']
        fixed_cents = params['process']['fixed_cents']

        data_proc = get_fixed_tiles(src_path, D, N, cell_type=cell_type, cell_size=grid_cell_size,
                deg_precision=deg_precision, land_cover_src_path=land_cover_src, land_cover_type=land_cover_type,
                include_history=include_history, min_days=min_days, min_detections=min_detections, percentile=percentile,
                fixed_cents=fixed_cents)
        loaders.save_pickle(data_proc, dest_path)
    elif process_type == 'create_table':
        data_proc = build_feature_table_src(src_path)
        logging.info("returned X and Y..")
        loaders.save_numpy(data_proc, dest_path)
    elif process_type == 'create_crf_features':
        data_proc = create_crf_features(params['process']['feature_grid_size'], src_path, params['process']['has_land_cover'])
        loaders.save_numpy(data_proc, dest_path)
    else:
        raise ValueError('Unknown process type "%s"' % process_type)

def build_feature_table_src(src_path):
    tile_dict = loaders.load_pickle(src_path)
    x_tiles, y_tiles, = tile_dict['x'], tile_dict['y']
    land_cover = tile_dict.get('land_cover', None)

    return build_feature_table(x_tiles, y_tiles, land_cover)

def build_feature_table(x_tiles, y_tiles, land_cover=None):
    N = int((len(x_tiles[0][0,0]) - len(y_tiles[0][0])) / 2)

    X_data, Y_data = [], []
    lb = preprocessing.LabelBinarizer()
    lb.fit([0, 1, 2])   # Do we need this?
    lb2 = preprocessing.LabelBinarizer()
    lb2.fit(range(17))
    # Convert all points to x-y pairs
    for i, (x_grid,y_grid) in enumerate(zip(x_tiles,y_tiles)):
        logging.debug("Processing cluster {}".format(i))
        T, H, W = y_grid.shape
        for t in range(T):
            for h in range(H):
                for w in range(W):
                    features = x_grid[t, :, h:h+2*N+1, w:w+2*N+1]   # get values for all previous days
                    features = lb.transform(features.flatten())
                    features = list(features.flatten())
                    if land_cover:
                        lc = land_cover[i][t, h-1, w-1]
                        lc = lb2.transform([lc])
                        lc = lc.flatten()
                        features += list(lc)   # append land cover feature

                    X_data.append(features)
                    if y_tiles:
                        Y_data.append(y_grid[t, h-1, w-1])

    # Convert to uint8 numpy arrays
    X_data = np.array(X_data).astype(np.uint8)
    y_data = np.array(Y_data).astype(np.uint8)

    return X_data, y_data


def get_fixed_tiles(src_path, D, N, cell_type='km', cell_size=1, deg_precision=.01, land_cover_src_path=None,
                    land_cover_type=None, include_history=False, min_days=2, min_detections=5, percentile=.98, fixed_cents=None):
    """Converts the data into fixed size windows for each cluster and each time-step."""
    tile_dict = {}

    data = pd.read_pickle(src_path)
    data = data[['date_local', 'cluster_id', 'lat', 'lon']]
    logging.debug('Successfully loaded fire data...')

    land_cover = None
    land_cover_tiles = []
    if land_cover_type=='deg':
        land_cover = LandCoverDeg(land_cover_src_path)
    elif land_cover_type=='km':
        land_cover = LandCoverMeters(land_cover_src_path)

    # Setup conversion funcs
    if cell_type=='km':
        units_to_lat = km_to_lat
        units_to_lon = km_to_lon
        lat_to_units = lat_to_km
        lon_to_units = lon_to_km
    elif cell_type=='deg':
        units_to_lat = deg_identity
        units_to_lon = deg_identity
        lat_to_units = deg_identity
        lon_to_units = deg_identity
    else:
        raise ValueError('Cell type "%s" is invalid' % cell_type)

    # Setup state values for fire history
    NEVER_ACTIVE = 0
    IS_ACTIVE = 2 if include_history else 1
    WAS_ACTIVE = 1 if include_history else NEVER_ACTIVE

    # Filter points based on # of days and total # of detections
    data_points = data.groupby('cluster_id').filter(lambda x: len(np.unique(x['date_local'])) >= min_days)
    data_points = data_points.groupby('cluster_id').filter(lambda x: x.shape[0] >= min_detections)
    cluster_groups = data_points.groupby('cluster_id')

    # Get the minimum and maximum latitudinal and longitudinal values for each cluster, for all times
    min_locs_deg = cluster_groups.min()[['lat', 'lon']]
    max_locs_deg = cluster_groups.max()[['lat', 'lon']]

    # Get the difference between minimum and maximum values of latitudinal and longitudinal values
    # for each cluster for all times
    lat_diffs_deg = max_locs_deg['lat'] - min_locs_deg['lat']
    lon_diffs_deg = max_locs_deg['lon'] - min_locs_deg['lon']

    # Compute diffs in specified units
    avg_lat_deg = (max_locs_deg['lat'] + min_locs_deg['lat']) / 2
    lat_diffs_units = lat_to_units(lat_diffs_deg) 
    lon_diffs_units = lon_to_units(lon_diffs_deg, avg_lat_deg)

    # Specify percentile of differences to choose as width and height of our bounding window
    height_units = round_to_value(lat_diffs_units.quantile(q=percentile), cell_size)
    width_units = round_to_value(lon_diffs_units.quantile(q=percentile), cell_size)

    height_cells = int(height_units / cell_size) 
    width_cells = int(width_units / cell_size)

    # Create tiles for each day for each cluster
    x = []
    y = []

    for i, (_, cluster) in enumerate(cluster_groups):
        # get all unique dates
        dates = list(np.unique(cluster['date_local']))
        dates.sort()
        logging.debug('Processing cluster {} (size {})'.format(i, len(dates)))

        x_clust = np.zeros((len(dates)-1, D, height_cells + 2*N, width_cells + 2*N))
        y_clust = np.zeros((len(dates)-1, height_cells, width_cells))

        if land_cover:
            land_cover_clust = np.zeros((len(dates)-1, height_cells, width_cells))

        for j, date in enumerate(dates[:-1]):

            # compute the centroid for the current day only
            observed_points = cluster[[cluster['date_local'].values == date][0]]

            if fixed_cents:
                centroid_lat = cluster['lat'].mean()
                centroid_lon = cluster['lon'].mean()
            else:
                centroid_lat = observed_points['lat'].mean()
                centroid_lon = observed_points['lon'].mean()

            # Round to align to lat/lon grid precision
            centroid_lat = round_to_value(centroid_lat, deg_precision)
            centroid_lon = round_to_value(centroid_lon, deg_precision)

            # === Compute grid for x (day j)
            # Center our bounding box at the centroid values of lat & long
            lat_offset = ((height_cells + 2*N) * units_to_lat(cell_size)) / 2
            lon_offset = ((width_cells + 2*N) * units_to_lon(cell_size, centroid_lat)) / 2

            lat_offset = round_to_value(lat_offset, deg_precision)
            lon_offset = round_to_value(lon_offset, deg_precision)

            top_left_lat = centroid_lat + lat_offset
            top_left_lon = centroid_lon - lon_offset

            history = np.zeros((height_cells + 2 * N, width_cells + 2 * N))
            history.fill(NEVER_ACTIVE)

            # NEVER_ACTIVE: grid cell doesn't have any history of fire activity.
            # WAS_ACTIVE: grid cell has a history of fire activity, but inactive today.
            # IS_ACTIVE: grid cell is on fire today...literally!!
            for k in range(j+1):
                observed_points = cluster[[cluster['date_local'].values == dates[k]][0]]
                if j < D:
                    x_clust[j, D - j + k - 1, :, :] = history
                elif j >= D and k > j - D:
                    x_clust[j, k - (j - D + 1), :, :] = history

                for _, fire in observed_points.iterrows():
                    # using the top-left corner, assign each observed point to one of the 1km x 1km grid cells
                    vert_dist_units = lat_to_units(top_left_lat - fire['lat'])
                    horiz_dist_units = lon_to_units(fire['lon'] - top_left_lon, fire['lat'])

                    vert_ind = int(round_to_value(vert_dist_units, cell_size) / cell_size)
                    horiz_ind = int(round_to_value(horiz_dist_units, cell_size) / cell_size)

                    if (vert_ind >= 0) and (horiz_ind >= 0) and (vert_ind < height_cells+2*N) and (horiz_ind < width_cells+2*N):
                        if j < D:
                            x_clust[j, D-j+k-1, vert_ind, horiz_ind] = IS_ACTIVE
                        elif j >= D and k > j-D:
                            x_clust[j, k-(j-D+1), vert_ind, horiz_ind] = IS_ACTIVE

                        history[vert_ind, horiz_ind] = WAS_ACTIVE
            if D > j:
                # add zero-padding
                x_clust[j, :(D-j)-1, :, :] = NEVER_ACTIVE

            # === Compute grid for y (day j+1)
            observed_points = cluster[[cluster['date_local'].values == dates[j+1]][0]]

            # center our bounding box at the centroid values of lat & long
            lat_offset = (height_cells * units_to_lat(cell_size)) / 2
            lon_offset = (width_cells * units_to_lon(cell_size, centroid_lat)) / 2

            lat_offset = round_to_value(lat_offset, deg_precision)
            lon_offset = round_to_value(lon_offset, deg_precision)

            top_left_lat = centroid_lat + lat_offset
            top_left_lon = centroid_lon - lon_offset

            for _, fire in observed_points.iterrows():
                vert_dist_units = lat_to_units(top_left_lat - fire['lat'])
                horiz_dist_units = lon_to_units(fire['lon'] - top_left_lon, fire['lat'])

                vert_ind = int(round_to_value(vert_dist_units, cell_size) / cell_size)
                horiz_ind = int(round_to_value(horiz_dist_units, cell_size) / cell_size)

                if (vert_ind >= 0) and (horiz_ind >= 0) and (vert_ind < height_cells) and (horiz_ind < width_cells):
                    y_clust[j, vert_ind, horiz_ind] = 1

            # === Land Cover
            if land_cover:
                land_cover_clust[j,:,:] = land_cover.get_tile(
                        top_left_lat, top_left_lon, height_cells, width_cells, cell_size, units_to_lat, units_to_lon)

        x.append(x_clust)
        y.append(y_clust)

        if land_cover:
            land_cover_tiles.append(land_cover_clust)

    tile_dict['x'] = x
    tile_dict['y'] = y

    if land_cover:
        tile_dict['land_cover'] = land_cover_tiles

    return tile_dict


# Creates a [t, width, height, num_features=9] array
# TODO: only handles full neighborhood- i.e. 9 surrounding pts. Might want to try with only above/below/left/right.
def create_crf_features(grid_size, src_path, have_land_cover):

    data_dict = loaders.load_pickle(src_path)
    #check if we have land cover
    if have_land_cover:    
        have_land_cover = True
        y = data_dict['y']
        x = data_dict['x']
        land_cover = data_dict['land_cover']
        #check that order of data_dict matches what I think it is
        #assert ['y', 'x', 'land_cover'] == data_dict.keys()
        land_cover = np.concatenate(np.array(land_cover), axis=0)
    else:
        have_land_cover = False
        #check that order of data_dict matches what I think it is
        #assert ['y', 'x'] == data_dict.keys()
        y = data_dict['y']
        x = data_dict['x']
    
    x_raw_data_all_days = np.concatenate(x)
    total_examples = x_raw_data_all_days.shape[0]
    y = np.concatenate(y)

    days_history = x_raw_data_all_days.shape[1]        
    if have_land_cover:
        num_features = days_history*grid_size*grid_size + 1 # features per y pixel is the number of grid cells times number of history days plus land cover
    else:
        num_features = days_history*grid_size*grid_size # features per y pixel is the number of grid cells time number of history days          
    x_features = 50*np.ones([total_examples,  x_raw_data_all_days.shape[2] - grid_size + 1, x_raw_data_all_days.shape[3] - grid_size + 1, num_features])   # change the height and width since the x_raw_data is 'padded' with extra data to allow computation of neighbors at all y pts.
    

    for day in range(days_history):
        x_raw_data = x_raw_data_all_days[:, day, :, :]
        for i in range(grid_size):
            for j in range(grid_size):
                if i != grid_size - 1 and j != grid_size - 1:
                    x_features[:, :, :, day*grid_size**2 + i + grid_size*j] = x_raw_data[:, i:i - grid_size + 1, j:j - grid_size + 1]
                elif i == grid_size - 1 and j != grid_size - 1:
                    x_features[:, :, :, day*grid_size**2 + i + grid_size*j] = x_raw_data[:, i:, j:j - grid_size + 1]
                elif i != grid_size - 1 and j == grid_size - 1:
                    x_features[:, :, :, day*grid_size**2 + i + grid_size*j] = x_raw_data[:, i:i - grid_size + 1, j:]
                elif i == grid_size - 1 and j == grid_size - 1:
                    x_features[:, :, :, day*grid_size**2 + i + grid_size*j] = x_raw_data[:, i:, j:]
    if have_land_cover:
        print x_features.shape
        print land_cover.shape
        x_features[:, :, :, -1] = land_cover
    if x_features.shape[-1] != num_features:
        print "shapes don't make sense for the created features"
        assert x_features.shape[-1] == num_features
    counter = collections.Counter(np.reshape(x_features, [-1]))
    # Make sure that I'm not just using default value of 50 for any entries.
    assert not(50 in counter.keys())

    x_features = x_features.astype(np.uint8)
    y = y.astype(np.uint8)

    return x_features, y

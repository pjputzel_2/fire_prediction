import os
import cPickle as pickle
import numpy as np

def save_pickle(data, dest_path):
    # Create directory if it doesn't exist
    dir_path = os.path.dirname(dest_path)
    if not os.path.exists(dir_path):
        os.makedirs(dir_path)

    with open(dest_path, "w") as f:
        pickle.dump(data, f, pickle.HIGHEST_PROTOCOL)

def load_pickle(src_path):
    print src_path
    with open(src_path, "r") as f:
        X = pickle.load(f)

    return X

def save_numpy(data, dest_path):
    # Create directory if it doesn't exist
    dir_path = os.path.dirname(dest_path)
    if not os.path.exists(dir_path):
        os.makedirs(dir_path)

    np.savez(dest_path, *data)

def load_numpy(src_path):
    data = np.load(src_path)

    keys = data.keys()
    keys.sort()

    data_list = []
    for k in keys:
        data_list.append(data[k])

    return tuple(data_list)

import yaml

def parse_params(params_path):
    with open(params_path, 'rb') as fin:
        params = yaml.safe_load(fin)

    params = parse_default_params(params)

    return params

def parse_default_params(params):
    default_params = {'data_dir': './data'}
    default_params.update(params)

    return default_params



from sklearn.metrics import f1_score

def compute_f1_scores(y, y_hat):
    f1_macro = f1_score(y, y_hat, average='macro')
    f1_micro = f1_score(y, y_hat, average='micro')
    f1_weighted = f1_score(y, y_hat, average='weighted')

    return {'f1_macro': f1_macro, 'f1_micro': f1_micro, 'f1_weighted': f1_weighted}


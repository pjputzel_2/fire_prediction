from pystruct.models import GridCRF #has additional dependencies of cvxopt
import pystruct.learners as ssvm
import numpy as np

class GridCRF(GridCRF):
    def __init__(self, params):
        inference_params = params['model']['inference_method'].get('params', [])
        inference_method_string = (params['model']['inference_method']['name'], {t[0]:t[1] for t in inference_params})

        super(GridCRF, self).__init__(neighborhood=params['model']['neighborhood'], inference_method=inference_method_string)
        self.ssvm = None
        self.ssvm_type = params['model']['ssvm_type']
        self.params = params
        self.inference_method = inference_method_string
        
    def fit(self, X, Y):
        if self.ssvm_type == 'OneSlack':
            self.ssvm = ssvm.OneSlackSSVM(model=self, C=100, inference_cache=100, tol=.1)
            self.ssvm.fit(X.astype(int), Y.astype(int))
        elif self.ssvm_type == 'Subgradient':
            self.ssvm = ssvm.SubgradientSSVM(model=self, batch_size = self.params['model']['batch_size'])
            self.ssvm.fit(X.astype(int), Y.astype(int))
        elif self.ssvm_type == 'FrankWolfe':
            self.ssvm = ssvm.FrankWolfeSSVM(model=self, max_iter=self.params['model']['max_iter'])
            self.ssvm.fit(X.astype(int), Y.astype(int))
        else:
            print 'Model type not found'
    def predict(self, X):
        y = self.ssvm.predict(X)
        y = np.reshape(np.array(y), [-1])

        return y
    


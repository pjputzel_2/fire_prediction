import numpy as np
from sklearn.linear_model import LogisticRegression

from utils.processing import build_feature_table

def reconstruct_cluster(pred, dates, height, width):
    cluster = np.array(pred)
    return np.reshape(cluster, (dates, height, width))

class LogisticRegression(LogisticRegression):
    def __init__(self, params):
        super(LogisticRegression, self).__init__()

    def predict_cluster(self, x_cluster, y_cluster, land_cover=None):
        X_data, _ = build_feature_table([x_cluster], [y_cluster], [land_cover])

        pred = self.predict(X_data)

        dates, height, width = y_cluster.shape
        pred_clust = reconstruct_cluster(pred, dates, height, width)

        return pred_clust

    def predict_cluster_prob(self, x_cluster, y_cluster, land_cover=None):
        X_data, _ = build_feature_table([x_cluster], [y_cluster], [land_cover])

        pred = self.predict_proba(X_data)[:,1]

        dates, height, width = y_cluster.shape
        pred_clust = reconstruct_cluster(pred, dates, height, width)

        return pred_clust

